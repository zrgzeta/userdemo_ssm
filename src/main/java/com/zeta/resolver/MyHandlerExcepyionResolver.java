package com.zeta.resolver;

import com.zeta.exception.MyException;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author zrg
 * @create 2021-04-30 17:16
 */
@Component
public class MyHandlerExcepyionResolver implements HandlerExceptionResolver {
    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        ModelAndView modelAndView = new ModelAndView();
        if (ex instanceof MyException){

            modelAndView.addObject("exInfo",ex);
            modelAndView.setViewName("error1");
        }

        return modelAndView;
    }
}
