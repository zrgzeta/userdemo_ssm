package com.zeta.service;

import com.zeta.domain.User;

import java.util.List;

/**
 * @author zrg
 * @create 2021-04-29 12:08
 */
public interface UserService {
    List<User> list();

    void save(User user,Long[] roleIds);

    Integer del(Long userId);

    User queryByUsernameAndPassword(String username, String password);
}
