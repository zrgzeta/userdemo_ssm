package com.zeta.service.impl;

import com.zeta.dao.RelationDao;
import com.zeta.dao.UserDao;
import com.zeta.domain.User;
import com.zeta.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Iterator;
import java.util.List;

/**
 * @author zrg
 * @create 2021-04-29 12:09
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;
    @Autowired
    private RelationDao relationDao;


    @Override
    public List<User> list() {

        List<User> userList = userDao.list();

        Iterator<User> iterator = userList.iterator();
        User user;
        while (iterator.hasNext()){
            user = iterator.next();
            List<String> roles = relationDao.queryRoleByUserId(user.getId());
            user.setRoleNames(roles);
        }

        return userList;
    }

    @Override
    public void save(User user, Long[] roleIds) {
        Long userId = userDao.save(user);
        for (long roleId : roleIds){
            relationDao.saveRelation(userId,roleId);
        }
    }

    @Override
    public Integer del(Long userId) {
        relationDao.del(userId);
        Integer del = userDao.del(userId);
        return del;
    }

    @Override
    public User queryByUsernameAndPassword(String username, String password) {
        return userDao.queryByUsernameAndPassword(username,password);

    }
}
