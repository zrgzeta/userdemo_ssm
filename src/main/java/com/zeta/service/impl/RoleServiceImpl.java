package com.zeta.service.impl;

import com.zeta.exception.MyException;
import com.zeta.dao.RoleDao;
import com.zeta.domain.Role;
import com.zeta.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zrg
 * @create 2021-04-29 17:18
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDao roleDao;

    /**
     * 查询所有角色
     * @return
     */
    @Override
    public List<Role> list() {
        return roleDao.list();
    }

    @Override
    public void deleteById(Long id) {
        roleDao.deleteById(id);
//        if (1 != 2){
//            throw new MyException();
//        }

//        try{
//            int i = 1 / 0;
//        }catch (Exception e){
//            throw new MyException();
//        }



    }

    @Override
    public Integer save(Role role) {
        return roleDao.save(role);
    }
}
