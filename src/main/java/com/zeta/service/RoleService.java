package com.zeta.service;

import com.zeta.exception.MyException;
import com.zeta.domain.Role;

import java.util.List;

/**
 * @author zrg
 * @create 2021-04-29 17:17
 */
public interface RoleService {
    /**
     * 查询所有
     * @return
     */
    List<Role> list();

    void deleteById(Long id);

    Integer save(Role role);
}
