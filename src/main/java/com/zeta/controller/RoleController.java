package com.zeta.controller;

import com.zeta.exception.MyException;
import com.zeta.domain.Role;
import com.zeta.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author zrg
 * @create 2021-04-29 17:14
 */
@Controller
@RequestMapping("/role")
public class RoleController {
    @Autowired
    private RoleService roleService;

    @RequestMapping("/save")
    public String save(Role role,Model model){
        Integer save = roleService.save(role);
        if (save == 1){
            model.addAttribute("info","添加成功~");
        }else {
            model.addAttribute("info","添加失败~");
        }
        return "role-add";
    }

    /**
     * 按照id删除角色
     * @param roleId
     * @return
     */
    @RequestMapping("/del")
    public String delRole(Long roleId) {
        roleService.deleteById(roleId);

        return "redirect:/role/list";
    }

    /**
     * 角色列表
     * @return
     */
    @RequestMapping("/list")
    public ModelAndView list(){
        ModelAndView modelAndView = new ModelAndView("role-list");
        List<Role> roles = roleService.list();
        modelAndView.addObject("roles",roles);
        return modelAndView;
    }
}
