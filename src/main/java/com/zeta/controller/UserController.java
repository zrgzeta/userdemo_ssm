package com.zeta.controller;

import com.sun.org.apache.xpath.internal.operations.Mod;
import com.zeta.domain.Role;
import com.zeta.domain.User;
import com.zeta.service.RoleService;
import com.zeta.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author zrg
 * @create 2021-04-29 11:02
 */
@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @RequestMapping("/login")
    public String login(User user, Model model, HttpSession session){
        User login = userService.queryByUsernameAndPassword(user.getUsername(),user.getPassword());
        if (login != null){
            session.setAttribute("loginUser",login);
            return "redirect:/index.jsp";
        }else {
            model.addAttribute("loginInfo","账号或者密码错误~");
            return "forward:/login.jsp";
        }

    }

    @RequestMapping("/del")
    public String del(Long userId){

        userService.del(userId);

        return "redirect:/user/list";
    }


    @RequestMapping("/save")
    public String save(User user,@RequestParam("roleId") Long[] roleIds){
        userService.save(user,roleIds);
        return "redirect:/user/saveUI";
    }

    @RequestMapping("/saveUI")
    public ModelAndView saveUI(){
        ModelAndView modelAndView = new ModelAndView("user-add");
        List<Role> roles = roleService.list();
        modelAndView.addObject("roles",roles);
        return modelAndView;
    }

    @RequestMapping("/list")
    public ModelAndView list(){
        ModelAndView modelAndView = new ModelAndView();
        List<User> users = userService.list();
        modelAndView.addObject("users",users);
        modelAndView.setViewName("user-list");
        return modelAndView;
    }
}
