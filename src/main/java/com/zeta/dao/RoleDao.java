package com.zeta.dao;

import com.zeta.domain.Role;

import java.util.List;

/**
 * @author zrg
 * @create 2021-04-29 17:22
 */
public interface RoleDao {
    List<Role> list();
    void deleteById(Long id);

    Integer save(Role role);
}
