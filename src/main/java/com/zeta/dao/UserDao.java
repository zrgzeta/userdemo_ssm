package com.zeta.dao;

import com.zeta.domain.User;

import java.util.List;

/**
 * @author zrg
 * @create 2021-04-29 10:58
 */
public interface UserDao {
    List<User> list();

    Long save(User user);

    Integer del(Long userId);

    User queryByUsernameAndPassword(String username, String password);
}
