package com.zeta.dao;

import java.util.List;

/**
 * @author zrg
 * @create 2021-05-02 12:41
 */
public interface RelationDao {
    List<String> queryRoleByUserId(Long userId);

    int saveRelation(Long userId, Long roleId);

    Integer del(Long userId);
}
