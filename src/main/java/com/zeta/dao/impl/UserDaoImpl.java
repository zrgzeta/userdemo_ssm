package com.zeta.dao.impl;

import com.zeta.dao.UserDao;
import com.zeta.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * @author zrg
 * @create 2021-04-29 10:59
 */
@Repository
public class UserDaoImpl implements UserDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<User> list() {
        return jdbcTemplate.query("SELECT id,username,email,phoneNum FROM sys_user",new BeanPropertyRowMapper<User>(User.class));
    }

    @Override
    public Long save(User user) {
        String sql = "INSERT INTO sys_user(username,`password`,email,phoneNum) VALUES(?,?,?,?)";
        PreparedStatementCreator psc = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement preparedStatement = connection.prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);
                preparedStatement.setString(1, user.getUsername());
                preparedStatement.setString(2, user.getPassword());
                preparedStatement.setString(3, user.getEmail());
                preparedStatement.setString(4, user.getPhoneNum());
                return preparedStatement;
            }
        };
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(psc, keyHolder);
        long userId = keyHolder.getKey().longValue();
        return userId;
    }

    @Override
    public Integer del(Long userId) {
        int del = jdbcTemplate.update("DELETE FROM sys_user WHERE id = ?", userId);
        return del;
    }

    @Override
    public User queryByUsernameAndPassword(String username, String password) {
        List<User> userList = jdbcTemplate.query("select * from sys_user where username = ? and password = ?", new BeanPropertyRowMapper<User>(User.class), username, password);
        if (userList.size() == 1){
            return userList.get(0);
        }
        return null;
    }
}
