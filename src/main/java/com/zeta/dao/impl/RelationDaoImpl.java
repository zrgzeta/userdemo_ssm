package com.zeta.dao.impl;

import com.zeta.dao.RelationDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author zrg
 * @create 2021-05-02 12:43
 */
@Repository
public class RelationDaoImpl implements RelationDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public List<String> queryRoleByUserId(Long userId) {
        List<String> stringList;
        stringList = jdbcTemplate.queryForList("SELECT roleName FROM sys_role,sys_user_role WHERE roleId = id AND userId = ?",String.class,userId);

        return stringList;
    }

    @Override
    public int saveRelation(Long userId, Long roleId) {
        String sql = "INSERT INTO sys_user_role VALUES(?,?)";
        return jdbcTemplate.update(sql,userId,roleId);
    }

    @Override
    public Integer del(Long userId) {
        int del = jdbcTemplate.update("DELETE FROM sys_user_role WHERE userId = ?", userId);
        return del;
    }
}
