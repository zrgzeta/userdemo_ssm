package com.zeta.dao.impl;

import com.zeta.dao.RoleDao;
import com.zeta.domain.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author zrg
 * @create 2021-04-29 17:23
 */
@Repository
public class RoleDaoImpl implements RoleDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * 查询所有的角色
     * @return
     */
    @Override
    public List<Role> list() {
        List<Role> roles = jdbcTemplate.query("select * from `sys_role`", new BeanPropertyRowMapper<Role>(Role.class));
        return roles;
    }

    /**
     * 按照id删除
     * @param id
     */
    @Override
    public void deleteById(Long id) {
        jdbcTemplate.update("delete from `sys_role` where id=?",id);
    }

    @Override
    public Integer save(Role role) {
        Integer insert = jdbcTemplate.update("insert into `sys_role`(roleName,roleDesc) values(?,?)",role.getRoleName(),role.getRoleDesc());
        return insert;
    }
}
